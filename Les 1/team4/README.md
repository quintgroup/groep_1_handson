# Team 4 
Er zijn meerdere klachten over de web-site [quintlabs.nl](http://quintlabs.nl), zo lijkt de tekst van de pagina wat gedateerd en is het midden juli niet echt meer de tijd voor een nieuwjaars wens en is het stijlhandboek aangepast met betrekking tot de hoe vaak je hetzelfde leesteken achter elkaar mag zetten, geef in het issue aan welk deel van de website je zou willen weghalen:
1. Open een issue, en geef aan welke wijziging jullie team voorstelt.
2. Bij assignee vul je je naam in, bij milestone selecteer de "1996 called, they ...",
3. Kijk even rond hoe je issue in de lijst staat (List in Issue menu aan linker zijde), kijk rond hoe de boards er uitzien en kijk bij de milestones welke al gedefinieerd zijn..
4. In de tussentijd krijg je antwoord op je issue en kan verder met opdracht 1.2