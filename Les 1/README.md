# Les 1 Hands-on introductie Gitlab

In de keten van voortbrenging is de code (als het om software ontwikkeling en configuratie gaat) de belangrijkste asset die meerdere keren door de keten vloeit (vandaar ook wel pipeline genoemd). Onderstaand voorbeeld laat deze stroom zien op basis van de functies van ontwikkelling via de centrale code opslag en het testen tot de uiteindelijke code deployment.

Gitlab werkt conceptueel gezien op basis van een 10-stappen workflow, van Idee tot Productie. Gitlab kan als een CI/CD suite ingezet worden en is hiermee een concurrent van o.a. Github en Atlassian, al kan Gitlab net als de concurrente ook voor delen van de keten goed overweg met tooling van derden.
![idea-to-production-10-steps](Les 1/idea-to-production-10-steps.png)

## Idea & Code Fase
Idee vorming kan je op verschillende manieren starten, bijvoorbeeld met onze Quint Design Thinking aanpak. Verder kan een Idee ontstaan tijdens een chat of email uitwisseling. De eerste stap na het identificeren van een Idee, is dat je het Idee onderbrengt in een **Issue** in Gitlab. Hiermee maak je de discussie en doorontwikkeling van het Idee inzichtelijk en traceerbaar. Hierbij maak je gebruik van de mogelijkheden om een kanban-stijl bord te gebruiken voor de Plan fase en maak je gebruik van issue tracking tools, om code of tekst aan het issue te koppelen in de Code fase. 

## Continuous Integration Fase
Op basis van het Issue ga je aan de slag met de code wijziging, waarbij je een Branch (afsplitsing) van de code maakt waarin jij werkt. Afhankelijk van het soort Issue kan er daarna **Commit** (opleveren van aangepaste code), een **Merge** (terugbrengen van de code naar de Master), een geautomatiseerde **Build** en **Test** fase doorlopen worden. Ook hierbij kan gebruik gemaakt worden van de Gitlab tooling om issue te rapporteren en of een oplossing voor te stellen.

## Delivery Fase
Daarna kan je de code vrijgeven voor staging en/of productie, waarbij het afhangt van de stijl die in de organisatie hanteert of er dan sprake is van Continuous Deployment (geautomatiseerde release) of meer handmatig controlle over de release en versies van de opgeleverde wijzigingen. 

Voor meer uitleg over de Gitlab Workflow kan je de volgende blog lezen: [gitlab workflow an overview](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)

# Opzet oefeningen
Je bent nu onderdeel van het web-ontwikkelteam van quintlabs.nl en binnen GitLab heb je de rol "Developer" gekregen. GitLab heeft een rol gebasseerd toegangsmodel, met passende functionaliteiten en rechten. Als eerste ga je kennismaken met de interface, je gaat een aantal opdrachten uitvoeren om hands-on ervaring op te doen voor continuous integration. In de opeenvolgende lessen vul je deze vaardigheden aan met een kijkje onder de motorkap van de automatisering van het landschap.

## Oefening 1.1
Maak je eerste Issue aan door het volgende uit te voeren in je eigen repository:
1. Aan de linkerkant kies je in het menu Issue (Tip: rechtsklik en open in nieuw tabblad, zodat je eenvoudig naar de team uitleg terug kan..)
2. Klik op New Issue (Groene knop "New Issue")
3. Geef het Issue een titel en een korte beschrijving, elk team heeft zijn eigen issue in de Team1, Team2, etc folder hierboven staan, klik op de folder en volg de instructies op.

## Oefening 1.2
Controleer wat de status van je issue is en volg de instructies op die als antwoord op je issue staan.

## Oefening 1.3 
Pas volgens opdracht de HTML code van de web-pagina aan, zoals aangegeven in het issue antwoord.

## Uitleg Branches en Mergers

Deployment uitleg en eindresultaat.

[Verder naar les 2](../Les 2)
